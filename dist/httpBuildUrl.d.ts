export declare type UrlPathParams = {
    [key: string]: string;
};
/**
 * Create an url. It replaces the placeholder in the url
 * with the value in options.
 *
 * Bsp.:
 * let url = 'hello/:world';
 * let opt = { world: 1};
 * httpBuildUrl(url, opt); // 'hello/1'
 *
 * @since 0.0.1
 * @param {string} url
 * @param {Object} urlPathParams
 *
 * @returns {string}
 */
export declare const httpBuildUrl: (url: string, urlPathParams?: UrlPathParams) => string;
