import { httpBuildUrl } from './httpBuildUrl';
import { httpBuildQuery } from './httpBuildQuery';
/**
 * @since 0.0.1
 * @param {String}        url
 * @param {UrlPathParams} urlPathParams
 * @param {UrlParams}     urlParams
 * @param {Number}        delay Time in ms
 */
export const httpRedirectTo = ({ url, urlPathParams = {}, urlParams = {}, delay = 0 }) => {
    let destination = httpBuildUrl(url, urlPathParams);
    if (Object.keys(urlParams).length > 0) {
        destination += httpBuildQuery(urlParams);
    }
    setTimeout(() => {
        // @ts-ignore
        window.location = destination;
    }, delay);
};
