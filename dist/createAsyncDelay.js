import { asyncTimeout } from './asyncTimeout';
function createAsyncDelay() {
    const start = Date.now();
    return {
        async wait(delay) {
            return asyncTimeout(delay);
        },
        async waitMin(delay) {
            const diff = Date.now() - start;
            if (diff >= delay) {
                return;
            }
            return this.wait(delay - diff);
        },
    };
}
;
export { createAsyncDelay };
