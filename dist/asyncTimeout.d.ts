declare function asyncTimeout(delay: number): Promise<void>;
export { asyncTimeout };
