/**
 * @since 0.0.1
 * @type {(v: PropertyKey) => boolean}
 */
export const has = Object.prototype.hasOwnProperty;
