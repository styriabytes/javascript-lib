export interface ConfirmFormSubmitActionParams {
    e: SubmitEvent;
    msg: string;
    fn?: Function | null;
}
/**
 * @since 0.0.1
 * @param {SubmitEvent} e
 * @param {String} msg
 * @param {Function|null} fn
 * @returns {boolean}
 */
export declare const confirmFormSubmitAction: ({ e, msg, fn }: ConfirmFormSubmitActionParams) => boolean;
