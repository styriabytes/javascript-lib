/**
 * Remove empty items from an array
 *
 * @since 0.0.1
 * @param {Array<any>} array The array that needs to be sorted out empty items
 * @returns {Array<any>} The array with no empty items
 */
export declare const removeEmptyItems: (array: Array<any>) => Array<any>;
