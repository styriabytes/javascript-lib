/**
 * Calls a function with a minimum of delay
 *
 * @since 0.0.1
 * @deprecated use AsyncDelay instead
 * @param {number}   refTime
 * @param {number}   minDelay
 * @param {Function} fn
 */
export const minDelay = (refTime, minDelay, fn) => {
    const diff = Date.now() - refTime;
    if (diff >= minDelay) {
        fn();
        return;
    }
    setTimeout(() => {
        fn();
    }, minDelay - diff);
};
