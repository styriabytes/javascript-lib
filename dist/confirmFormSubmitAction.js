import { isFunction } from 'lodash-es';
/**
 * @since 0.0.1
 * @param {SubmitEvent} e
 * @param {String} msg
 * @param {Function|null} fn
 * @returns {boolean}
 */
export const confirmFormSubmitAction = ({ e, msg, fn = null }) => {
    e.preventDefault();
    if (!window.confirm(msg)) {
        if (isFunction(fn)) {
            fn(e);
        }
        return false;
    }
    e.target.submit();
    return true;
};
