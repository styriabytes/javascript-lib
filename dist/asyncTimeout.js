function asyncTimeout(delay) {
    return new Promise((resolve) => {
        window.setTimeout(() => {
            resolve();
        }, delay);
    });
}
;
export { asyncTimeout };
