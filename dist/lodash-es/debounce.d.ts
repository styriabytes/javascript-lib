import { debounce, DebouncedFunc, DebounceSettings } from "lodash-es";
export { DebounceSettings, DebouncedFunc };
export default debounce;
