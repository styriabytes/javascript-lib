import { isBuffer } from "lodash-es";
export default isBuffer;
