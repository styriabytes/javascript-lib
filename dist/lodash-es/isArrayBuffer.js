import { isArrayBuffer } from "lodash-es";
export default isArrayBuffer;
