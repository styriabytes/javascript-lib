import { isSafeInteger } from "lodash-es";
export default isSafeInteger;
