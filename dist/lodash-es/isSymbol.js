import { isSymbol } from "lodash-es";
export default isSymbol;
