import { isTypedArray } from "lodash-es";
export default isTypedArray;
