import { isBoolean } from "lodash-es";
export default isBoolean;
