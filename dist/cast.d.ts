export interface CastArguments {
    value: any;
    primitive: Function;
    nullable?: boolean;
}
/**
 * @since 0.0.1
 * @param {any}      value
 * @param {Function} primitive Javascript primitives (String, Number, Boolean, Array)
 * @param {boolean}  nullable
 * @return {any}
 */
export declare const cast: ({ value, primitive, nullable }: CastArguments) => any;
export declare const castStringNull: (value: any, nullable?: boolean) => string;
export declare const castString: (value: any) => string;
export declare const castNumberNull: (value: any, nullable?: boolean) => number;
export declare const castNumber: (value: any) => number;
export declare const castBooleanNull: (value: any, nullable?: boolean) => boolean;
export declare const castBoolean: (value: any) => boolean;
export declare const castArrayNull: (value: any, nullable?: boolean) => Array<any>;
export declare const castArray: (value: any) => Array<any>;
