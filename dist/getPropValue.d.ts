/**
 * @since 0.0.1
 * @param {{[key: string]: any}} obj Target Object
 * @param {string}               prop Object property name
 * @param {any}                  def Default value
 *
 * @return {any}
 */
export declare const getPropValue: (obj: {
    [key: string]: any;
}, prop: string, def?: any) => any;
