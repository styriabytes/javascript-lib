/**
 * Create an url. It replaces the placeholder in the url
 * with the value in options.
 *
 * Bsp.:
 * let url = 'hello/:world';
 * let opt = { world: 1};
 * httpBuildUrl(url, opt); // 'hello/1'
 *
 * @since 0.0.1
 * @param {string} url
 * @param {Object} urlPathParams
 *
 * @returns {string}
 */
export const httpBuildUrl = (url, urlPathParams = {}) => {
    Object.keys(urlPathParams).forEach((item) => {
        url = url.replace(new RegExp(`:${item}`), urlPathParams[item]);
    });
    return url;
};
