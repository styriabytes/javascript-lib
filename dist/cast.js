import { inArray } from "./inArray";
import { isArray } from "lodash-es";
const trueValues = [
    'true',
    'on',
];
const falseValues = [
    'false',
    'off',
];
/**
 * @since 0.0.1
 * @param {any}      value
 * @param {Function} primitive Javascript primitives (String, Number, Boolean, Array)
 * @param {boolean}  nullable
 * @return {any}
 */
export const cast = ({ value = null, primitive, nullable = false }) => {
    if (nullable && value === null) {
        return null;
    }
    if (primitive === Array) {
        if (value === null) {
            return [];
        }
        if (isArray(value)) {
            return value;
        }
        return [value];
    }
    if (primitive === Boolean) {
        if (inArray(value, trueValues)) {
            return true;
        }
        if (inArray(value, falseValues)) {
            return false;
        }
    }
    return primitive(value);
};
export const castStringNull = (value, nullable = true) => cast({ value, primitive: String, nullable });
export const castString = (value) => castStringNull(value, false);
export const castNumberNull = (value, nullable = true) => cast({ value, primitive: Number, nullable });
export const castNumber = (value) => castNumberNull(value, false);
export const castBooleanNull = (value, nullable = true) => cast({ value, primitive: Boolean, nullable });
export const castBoolean = (value) => castBooleanNull(value, false);
export const castArrayNull = (value, nullable = true) => cast({ value, primitive: Array, nullable });
export const castArray = (value) => castArrayNull(value, false);
