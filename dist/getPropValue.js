import { has } from './has';
/**
 * @since 0.0.1
 * @param {{[key: string]: any}} obj Target Object
 * @param {string}               prop Object property name
 * @param {any}                  def Default value
 *
 * @return {any}
 */
export const getPropValue = (obj, prop, def = null) => has.call(obj, prop) ? obj[prop] : def;
