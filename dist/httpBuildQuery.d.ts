export interface UrlParams {
    [key: string]: string;
}
/**
 * @since 0.0.1
 * @param {UrlParams} urlParams
 *
 * @returns {string}
 */
export declare const httpBuildQuery: (urlParams: UrlParams) => string;
