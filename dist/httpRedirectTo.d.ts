import { UrlPathParams } from './httpBuildUrl';
import { UrlParams } from './httpBuildQuery';
export declare type RedirectToParams = {
    url: string;
    urlPathParams?: UrlPathParams;
    urlParams?: UrlParams;
    delay?: number;
};
/**
 * @since 0.0.1
 * @param {String}        url
 * @param {UrlPathParams} urlPathParams
 * @param {UrlParams}     urlParams
 * @param {Number}        delay Time in ms
 */
export declare const httpRedirectTo: ({ url, urlPathParams, urlParams, delay }: RedirectToParams) => void;
