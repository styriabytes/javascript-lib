/**
 * @since 0.0.1
 * @type {(v: PropertyKey) => boolean}
 */
export declare const has: (v: PropertyKey) => boolean;
