import {isFunction} from 'lodash-es';

export interface ConfirmFormSubmitActionParams {
    e: SubmitEvent
    msg: string
    fn?: Function | null
}

/**
 * @since 0.0.1
 * @param {SubmitEvent} e
 * @param {String} msg
 * @param {Function|null} fn
 * @returns {boolean}
 */
export const confirmFormSubmitAction = ({e, msg, fn = null}: ConfirmFormSubmitActionParams): boolean => {
    e.preventDefault();
    if (!window.confirm(msg)) {
        if (isFunction(fn)) {
            fn(e);
        }
        return false;
    }
    (e.target as HTMLFormElement).submit();
    return true;
};
