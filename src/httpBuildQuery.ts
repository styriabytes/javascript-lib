export interface UrlParams {
    [key: string]: string
}

/**
 * @since 0.0.1
 * @param {UrlParams} urlParams
 *
 * @returns {string}
 */
export const httpBuildQuery = (urlParams: UrlParams): string => {
    const keys = Object.keys(urlParams);
    if (!keys.length) {
        return '';
    }
    let params = '?';
    keys.forEach((item) => {
        params += item + '=' + urlParams[item] + '&';
    });
    return params.slice(0, -1); // Remove the last '&'
}
