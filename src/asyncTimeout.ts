function asyncTimeout(delay: number): Promise<void> {
    return new Promise((resolve) => {
        window.setTimeout(() => {
            resolve();
        }, delay);
    });
};

export { asyncTimeout }
