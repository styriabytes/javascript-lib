/**
 * Remove empty items from an array
 *
 * @since 0.0.1
 * @param {Array<any>} array The array that needs to be sorted out empty items
 * @returns {Array<any>} The array with no empty items
 */
export const removeEmptyItems = (array: Array<any>): Array<any> => {
    return !array.length ? array : array.filter((item) => {
        if (item) {
            return item;
        }
    });
}
