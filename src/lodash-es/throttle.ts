import { throttle, ThrottleSettings } from "lodash-es";

export { ThrottleSettings };
export default throttle;
