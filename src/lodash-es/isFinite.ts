import { isFinite } from "lodash-es";
export default isFinite;
