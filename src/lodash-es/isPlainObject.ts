import { isPlainObject } from "lodash-es";
export default isPlainObject;
