import { isInteger } from "lodash-es";
export default isInteger;
