/**
 * Calls a function with a minimum of delay
 *
 * @since 0.0.1
 * @deprecated use AsyncDelay instead
 * @param {number}   refTime
 * @param {number}   minDelay
 * @param {Function} fn
 */
export const minDelay = (refTime: number, minDelay: number, fn: Function): void => {
    const diff = Date.now() - refTime;
    if (diff >= minDelay) {
        fn();
        return;
    }
    setTimeout(() => {
        fn();
    }, minDelay - diff);
}
