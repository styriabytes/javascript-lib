import { AsyncDelay } from './AsyncDelay';
import { asyncTimeout } from './asyncTimeout';

function createAsyncDelay(): AsyncDelay {
    
    const start = Date.now();
    
    return {
        async wait(delay: number): Promise<void> {
            return asyncTimeout(delay);
        },
        async waitMin(delay: number): Promise<void> {
            const diff = Date.now() - start;
            if (diff >= delay) {
                return;
            }
            return this.wait(delay - diff);
        },
    };
};

export { createAsyncDelay }
