import {has} from './has';

/**
 * @since 0.0.1
 * @param {{[key: string]: any}} obj Target Object
 * @param {string}               prop Object property name
 * @param {any}                  def Default value
 *
 * @return {any}
 */
export const getPropValue = (obj: {[key: string]: any}, prop: string, def: any = null): any => has.call(obj, prop) ? obj[prop] : def;
