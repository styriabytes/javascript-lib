interface TimeToggler {
    isOn: boolean
    isOff: boolean
    toggler(time?: number): void
}

export { TimeToggler }