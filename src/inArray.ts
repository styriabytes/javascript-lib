/**
 * Check if a value exists in an array
 *
 * @since 0.0.1
 * @param {any}        value
 * @param {Array<any>} array
 *
 * @returns {boolean}
 */
export const inArray = (value: any, array:Array<any>):boolean => {
    return array.indexOf(value) !== -1;
}
