interface AsyncDelay {
    wait: (delay: number) => Promise<void>
    waitMin: (delay: number) => Promise<void>
}

export { AsyncDelay }
