import {httpBuildUrl, UrlPathParams} from './httpBuildUrl';
import {httpBuildQuery, UrlParams} from './httpBuildQuery';

export type RedirectToParams = {
    url: string
    urlPathParams?: UrlPathParams
    urlParams?: UrlParams
    delay?: number
}

/**
 * @since 0.0.1
 * @param {String}        url
 * @param {UrlPathParams} urlPathParams
 * @param {UrlParams}     urlParams
 * @param {Number}        delay Time in ms
 */
export const httpRedirectTo = ({url, urlPathParams = {}, urlParams = {}, delay = 0}: RedirectToParams): void => {
    let destination = httpBuildUrl(url, urlPathParams);
    if (Object.keys(urlParams).length > 0) {
        destination += httpBuildQuery(urlParams);
    }
    setTimeout(() => {
        // @ts-ignore
        window.location = destination;
    }, delay);
}
