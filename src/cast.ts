import {inArray} from "./inArray";
import {isArray} from "lodash-es";

export interface CastArguments {
    value: any,
    primitive: Function
    nullable?: boolean
}

const trueValues = [
    'true',
    'on',
];
const falseValues = [
    'false',
    'off',
];

/**
 * @since 0.0.1
 * @param {any}      value
 * @param {Function} primitive Javascript primitives (String, Number, Boolean, Array)
 * @param {boolean}  nullable
 * @return {any}
 */
export const cast = ({value = null, primitive, nullable = false}: CastArguments): any => {
    if (nullable && value === null) {
        return null;
    }
    if (primitive === Array) {
        if (value === null) {
            return [];
        }
        if (isArray(value)) {
            return value;
        }
        return [value];
    }
    if (primitive === Boolean) {
        if (inArray(value, trueValues)) {
            return true;
        }
        if (inArray(value, falseValues)) {
            return false;
        }
    }
    return primitive(value);
}

export const castStringNull = (value: any, nullable: boolean = true): string => cast({value, primitive: String, nullable});
export const castString = (value: any): string => castStringNull(value, false);

export const castNumberNull = (value: any, nullable: boolean = true): number => cast({value, primitive: Number, nullable});
export const castNumber = (value: any): number => castNumberNull(value, false);

export const castBooleanNull = (value: any, nullable: boolean = true): boolean => cast({value, primitive: Boolean, nullable});
export const castBoolean = (value: any): boolean => castBooleanNull(value, false);

export const castArrayNull = (value: any, nullable: boolean = true): Array<any> => cast({value, primitive: Array, nullable});
export const castArray = (value: any): Array<any> => castArrayNull(value, false);
