import { asyncTimeout } from "./asyncTimeout";
import { TimeToggler } from "./TimeToggler";

function createTimeToggler(): TimeToggler {
    return {
        isOn: false,
        isOff: true,
        toggle(time = 1000): void {
            this.isOn = true;
            this.isOff = false;
            asyncTimeout(time).then(() => {
                this.isOn = false;
                this.isOff = true;
            });
        },
    };
}

export { createTimeToggler };