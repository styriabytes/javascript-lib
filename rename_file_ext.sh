#!/usr/bin/env bash

target_dir="src/lodash-es"


echo "Copy files from @types/lodash-es"
cp node_modules/@types/lodash-es/*.d.ts "$target_dir"


echo "Change file extensions"
cd "$target_dir" || exit
old_ext="d.ts"
new_ext="ts"
echo "$target_dir, $old_ext, $new_ext"
for file in *.$old_ext
do
    mv -v "$file" "${file%.$old_ext}.$new_ext"
done;


echo "Change import from 'lodash' to 'lodash-es'"
sed -i '' -e 's/lodash/lodash\-es/g' ./*.ts
