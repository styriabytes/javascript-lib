import { assert } from 'assertthat';
import { cast } from '../../src/cast';

suite('cast', (): void => {

    test('casts an null value to an number value when nullable flag is false', async (): Promise<void> => {
        const castedValue = cast({ value: null, primitive: Number, nullable: false });

        assert.that(castedValue).is.equalTo(0);
    });

    test('returns null when null value is given and when nullable flag is true', async (): Promise<void> => {
        const castedValue = cast({ value: null, primitive: Number, nullable: true });

        assert.that(castedValue).is.equalTo(null);
    });

    test('casts an string value to an number value', async (): Promise<void> => {
        const castedValue = cast({ value: '14.4', primitive: Number });

        assert.that(castedValue).is.equalTo(14.4);
    });

    test('casts an number value to an string value', async (): Promise<void> => {
        const castedValue = cast({ value: 4.4, primitive: String });

        assert.that(castedValue).is.equalTo('4.4');
    });
});
